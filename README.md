
# 功能
1) 账号密码存储， 加密存储
2) 登录二次验证 MFA 
3) 主要模型： 
    密码组管理
    密码条目管理（自定义秘钥加密存储）
    自定义字段
    访问日志审计
4) 菜单：
    密码管理
    操作日志
5) 双击复制密码\用户名\url

# Install
1) git clone https://gitee.com/zhugaotao/web-keepass.git
2) python > 3.5 and mariadb(mysql)> 5.5
3) pip install -r requirements.txt
4) 修改配置文件webKeepass/settings.py

![图片](https://gitee.com/zhugaotao/web-keepass/raw/master/static/image/1.jpg)
![图片](https://gitee.com/zhugaotao/web-keepass/raw/master/static/image/2.jpg)
![图片](https://gitee.com/zhugaotao/web-keepass/raw/master/static/image/3.jpg)
![图片](https://gitee.com/zhugaotao/web-keepass/raw/master/static/image/4.jpg)



# TODO:
   1) 密码列表增加checkbox功能
   2) 优化fields页面显示，使用模态框
   3) 用户自助修改用户名密码及配置头像
   4) ~~同一个ip只能尝试登陆6次，不然就锁60分钟~~
   5) ~~操作日志~~
   6) ~~登陆审计，10分钟内3次失败则暂停访问~~

