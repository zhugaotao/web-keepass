# Generated by Django 3.0.8 on 2020-10-01 05:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0004_auto_20200922_2303'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='syslog',
            options={'ordering': ['created_at'], 'verbose_name': '用户行为记录', 'verbose_name_plural': '用户行为记录'},
        ),
        migrations.AlterField(
            model_name='syslog',
            name='action',
            field=models.CharField(choices=[(1, 'Login'), (2, 'ItemGroup'), (3, 'Item'), (4, 'Fields')], default=1, max_length=50, verbose_name='动作'),
        ),
        migrations.AlterField(
            model_name='syslog',
            name='message',
            field=models.TextField(blank=True, null=True, verbose_name='信息'),
        ),
    ]
